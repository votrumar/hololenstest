﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClick : MonoBehaviour
{

    // color after odd number of clicks
    public Color oddColor;
    // color after even number of clicks
    public Color evenColor;

    private int _clickCount;

    void Start()
    {
        oddColor = Color.red;
        evenColor = Color.green;
        _clickCount = 0;
        UpdateColor();
    }

    public void ChangeColor()
    {
        _clickCount++;
        UpdateColor();
    }

    private void UpdateColor()
    {
        var rend = GetComponent<Renderer>();
        if (rend == null)
            return;
        rend.material.color = _clickCount % 2 == 0 ? oddColor : evenColor;
    }
}
